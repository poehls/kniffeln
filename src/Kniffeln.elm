module Kniffeln exposing (main)

import Array exposing (Array)
import Browser
import Html exposing (Html, button, div, h1, p, span, text)
import Html.Attributes exposing (class, classList, disabled)
import Html.Events exposing (onClick)
import Random



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = initModel
        , subscriptions = subscriptions
        , view = view
        , update = update
        }



-- MODEL


type alias Model =
    { board :
        { ones : Int
        , twos : Int
        , fullHouse : Int
        }
    , dice : FiveDice
    }


initModel : () -> ( Model, Cmd Msg )
initModel _ =
    ( Model
        { ones = -1
        , twos = -1
        , fullHouse = -1
        }
        Array.empty
    , Random.generate NewDieFaces roll5
    )


type alias FiveDice =
    Array Die


type alias Die =
    { face : Int
    , selected : Selected
    }


type alias Selected =
    Bool


newDie : Int -> Die
newDie face =
    Die face False



-- UPDATE


type Msg
    = RollDice
    | NewDieFaces FiveDice
    | UpdateDie { die : Die, i : Int }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RollDice ->
            ( model
            , Random.generate NewDieFaces roll5
            )

        --TODO
        NewDieFaces faces ->
            ( { model | dice = faces }
            , Cmd.none
            )

        UpdateDie { die, i } ->
            let
                newDice =
                    Array.set i die model.dice
            in
            ( { model | dice = newDice }
            , Cmd.none
            )


rollDie : Random.Generator Die
rollDie =
    Random.map newDie (Random.int 1 6)


roll5 : Random.Generator FiveDice
roll5 =
    Random.map5 array5 rollDie rollDie rollDie rollDie rollDie



-- list1 : a -> List a
-- list1 arg1 =
--     [ arg1 ]
-- list2 : a -> a -> List a
-- list2 arg1 arg2 =
--     [ arg1, arg2 ]
-- list3 : a -> a -> a -> List a
-- list3 arg1 arg2 arg3 =
--     [ arg1, arg2, arg3 ]
-- list4 : a -> a -> a -> a -> List a
-- list4 arg1 arg2 arg3 arg4 =
--     [ arg1, arg2, arg3, arg4 ]
-- list5 : a -> a -> a -> a -> a -> List a
-- list5 arg1 arg2 arg3 arg4 arg5 =
--     [ arg1, arg2, arg3, arg4, arg5 ]


array5 : a -> a -> a -> a -> a -> Array a
array5 arg1 arg2 arg3 arg4 arg5 =
    Array.fromList [ arg1, arg2, arg3, arg4, arg5 ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div
        [ class "app-container"
        ]
        [ viewHeader
        , div []
            [ viewScoreBoard
            , viewControlPanel model
            ]
        ]


viewHeader : Html Msg
viewHeader =
    div
        [ class "header"
        ]
        [ h1
            [ class "title-font" ]
            [ span
                [ class "title-font-accent" ]
                [ text "function "
                ]
            , text "k(n){if(f)el(n)}"
            ]
        ]


viewScoreBoard : Html Msg
viewScoreBoard =
    div
        [ class "score-board"
        ]
        [ p
            [ class "text-left"
            ]
            [ text "|--- SCORE BOARD goes from here ..."
            ]
        , p
            [ class "text-right"
            ]
            [ text "... to here ---|"
            ]
        ]


viewControlPanel : Model -> Html Msg
viewControlPanel model =
    div
        [ class "control-panel"
        ]
        [ viewDiceArea model.dice
        , viewButtonArea model
        ]


viewDiceArea : FiveDice -> Html Msg
viewDiceArea dice =
    div
        [ class "dice-area"
        ]
        [ div [ class "dice-wrapper" ]
            ([ p
                [ class "text-left"
                ]
                [ text "|--- DICE AREA goes from here ..."
                ]
             , p
                [ class "text-right"
                ]
                [ text "... to here ---|"
                ]
             ]
                ++ List.indexedMap
                    viewDieField
                    (Array.toList dice)
            )
        , p
            [ classList
                [ ( "text-instruction", True )
                , ( "text-center", True )
                ]
            ]
            [ text "Select dice to reroll" ]
        ]


viewDieField : Int -> Die -> Html Msg
viewDieField idx die =
    div
        [ classList
            [ ( "die", True )
            , ( "die-selected", die.selected )
            ]
        , onClick (UpdateDie { die = { die | selected = not die.selected }, i = idx })
        ]
        [ text <| String.fromInt die.face ]


viewButtonArea : Model -> Html Msg
viewButtonArea model =
    div
        [ classList
            [ ( "button-area", True ) ]
        ]
        [ p
            [ class "text-left"
            ]
            [ text "|--- BUTTON AREA goes from here ..."
            ]
        , p
            [ class "text-right"
            ]
            [ text "... to here ---|"
            ]
        , div
            [ class "center-container" ]
            [ button
                [ onClick RollDice
                , disabled <| List.all not <| List.map .selected <| Array.toList <| model.dice
                , class "btn-flat"
                ]
                [ text "Roll the selected dice!"
                ]
            ]
        ]
